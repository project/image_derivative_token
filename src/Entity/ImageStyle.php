<?php

namespace Drupal\image_derivative_token\Entity;

use Drupal\image\Entity\ImageStyle as ImageStyleParent;

/**
 * Defines a new ImageStyle class based on the original.
 */
class ImageStyle extends ImageStyleParent {

  /**
   * {@inheritdoc}
   */
  public function buildUrl($path, $clean_urls = NULL) {
    // If the "suppress_itok_output" setting has been enabled for this image
    // style, suppress the image token from being output.
    if ($this->getThirdPartySetting('image_derivative_token', 'suppress_itok_output', FALSE)) {
      \Drupal::config('image.settings')->setModuleOverride(['suppress_itok_output' => TRUE]);
    }

    return parent::buildUrl($path, $clean_urls);
  }

}
