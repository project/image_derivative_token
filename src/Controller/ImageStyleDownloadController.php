<?php

namespace Drupal\image_derivative_token\Controller;

use Drupal\image\Controller\ImageStyleDownloadController as ImageStyleDownloadControllerParent;
use Drupal\image\ImageStyleInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a new ImageStyleDownloadController class based on the original.
 */
class ImageStyleDownloadController extends ImageStyleDownloadControllerParent {

  /**
   * {@inheritdoc}
   */
  public function deliver(Request $request, $scheme, ImageStyleInterface $image_style) {
    // If the "allow_insecure_derivatives" setting has been enabled for this
    // image style, allow the image derivative to be generated without a token.
    if ($image_style->getThirdPartySetting('image_derivative_token', 'allow_insecure_derivatives', FALSE)) {
      $this->config('image.settings')->setModuleOverride(['allow_insecure_derivatives' => TRUE]);
    }

    return parent::deliver($request, $scheme, $image_style);
  }

}
