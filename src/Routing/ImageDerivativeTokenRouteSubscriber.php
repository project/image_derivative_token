<?php

namespace Drupal\image_derivative_token\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * ImageDerivativeTokenRouteSubscriber route subscriber.
 */
class ImageDerivativeTokenRouteSubscriber extends RouteSubscriberBase {

  /**
   * Alter the default controller used by the "image.style_public" route.
   *
   * @param \Symfony\Component\Routing\RouteCollection $collection
   *   The route collection.
   */
  public function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('image.style_public')) {
      $route->setDefault('_controller', '\Drupal\image_derivative_token\Controller\ImageStyleDownloadController::deliver');
    }
  }

}
