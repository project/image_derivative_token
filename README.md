# Image Derivative Token

## Introduction

Image Derivative Token provides the ability for site administrators to allow
image derivatives to be generated for specific image styles without a valid
image token.

## Requirements

This module requires the Image module provided by Drupal core.

## Installation

The recommended method of installing this module is with Composer:

`composer require drupal/image_derivative_token`

## Permissions

Add the "Administer image derivative token settings" permission to any roles
that should be able to configure an image style to allow image derivatives
to be generated without a valid image token.

## Configuration

When adding or editing an image style, select the "Allow insecure derivatives"
checkbox to allow image derivatives to be generated without a valid image
token.
